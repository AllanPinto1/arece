import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MdMenuModule,MaterialModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes} from '@angular/router';

import { AppComponent } from './Components/Component/app.component';
import { Header } from './Components/Header/header';
import { Footer } from './Components/Footer/footer';
import { Accueil } from './Components/Accueil/accueil';
import { About } from './Components/About/about';
import { Contact } from './Components/Contact/contact';
import { Documentation } from './Components/Documentation/documentation';
import { Diagnosol } from './Components/Diagnosol/diagnosol';
import { Ciesst } from './Components/Ciesst/ciesst';
import { Calsimeol } from './Components/Calsimeol/calsimeol';

const appRoutes: Routes = [
  {path: 'accueil', component: Accueil},
  {path: 'documentation', component: Documentation},
  {path: 'about', component: About},
  {path: 'contact', component: Contact},
  {path: 'diagnosol', component: Diagnosol},
  {path: 'ciesst', component: Ciesst},
  {path: 'calsimeol', component: Calsimeol}
];


@NgModule({
  declarations: [
    AppComponent,Header,Footer,
    Accueil,Documentation,About,Contact,
    Diagnosol,Ciesst,Calsimeol
  ],
  imports: [
    BrowserModule,MaterialModule,NoopAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent,Header]
})
export class AppModule { }
