import { Component } from '@angular/core';
import { Header } from '../Header/header';
import { Footer } from '../Footer/footer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
}
