import { Component, ViewChild } from '@angular/core';


@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.html',
  styleUrls: ['./documentation.css']
})
export class Documentation {
  title = 'Documentation';
}
