import { Component, ViewChild } from '@angular/core';


@Component({
  selector: 'app-about',
  templateUrl: './about.html',
  styleUrls: ['./about.css']
})
export class About {
  title = 'About';
}
