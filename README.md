# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository follow the development of the website ARECE, which is a platform gathering different websites on green energies, such as Calsimeol, EolAtlas, CIESST & Diagnosol

### How do I get set up? ###

To get the last change, you need to pull it :
Start by "Fetch" ("Rapatrier") to see the last modifications made on the repository.
To confirm, press then "Pull" ("Récupérer") to get those modifications on your local repository and be up to date.

To make modifications or add new files, you need to push it :
Start by "Commit" ("Valider") to select the files that you modified or added, then add a description of your update and confirm.
Confirm your modification by pressing "Push" ("Envoyer")



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact